[![Code
Climate](https://lima.codeclimate.com/github/dzencot/project-lvl3-s71/badges/gpa.svg)](https://lima.codeclimate.com/github/dzencot/project-lvl3-s71)
[![Test
Coverage](https://lima.codeclimate.com/github/dzencot/project-lvl3-s71/badges/coverage.svg)](https://lima.codeclimate.com/github/dzencot/project-lvl3-s71/coverage)
[![Issue
Count](https://lima.codeclimate.com/github/dzencot/project-lvl3-s71/badges/issue_count.svg)](https://lima.codeclimate.com/github/dzencot/project-lvl3-s71)
[![Build
Status](https://travis-ci.org/dzencot/project-lvl3-s71.svg?branch=master)](https://travis-ci.org/dzencot/project-lvl3-s71)
# Hexlet project L3 #
## Install: ##
```
  git clone https://github.com/dzencot/project-lvl3-s71 page_loader
  cd page_loader
  make install
```
## Usage: ##
```
  make run 1=http://your_page
```
